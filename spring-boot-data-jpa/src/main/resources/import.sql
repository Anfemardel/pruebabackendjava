/* Populate tables */
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('Andres', 'Guzman', 'profesor@bolsadeideas.com', '2017-08-01', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('John', 'Doe', 'john.doe@gmail.com', '2017-08-02', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('Linus', 'Torvalds', 'linus.torvalds@gmail.com', '2017-08-03', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('Jane', 'Doe', 'jane.doe@gmail.com', '2017-08-04', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('Rasmus', 'Lerdorf', 'rasmus.lerdorf@gmail.com', '2017-08-05', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('Erich', 'Gamma', 'erich.gamma@gmail.com', '2017-08-06', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('Richard', 'Helm', 'richard.helm@gmail.com', '2017-08-07', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('Ralph', 'Johnson', 'ralph.johnson@gmail.com', '2017-08-08', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('John', 'Vlissides', 'john.vlissides@gmail.com', '2017-08-09', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('James', 'Gosling', 'james.gosling@gmail.com', '2017-08-010', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('Bruce', 'Lee', 'bruce.lee@gmail.com', '2017-08-11', '');
INSERT INTO clientes (nombre, apellido, email, create_at, foto) VALUES('Johnny', 'Doe', 'johnny.doe@gmail.com', '2017-08-12', '');


/* Populate tabla productos */
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('TIGO-DATOS', 'DATOS',259990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('TIGO-VOZ', 'VOZ',123490, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('TIGO-HOGAR', 'HOGAR',1499990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('MOVISTAR-HOGAR', 'HOGAR',37990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('MOVISTAR-VOZ', 'VOZ',69990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('MOVISTAR-DATOS', 'DATOS',69990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('COMCEL-VOZ', 'VOZ',299990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('COMCEL-DATOS', 'DATOS',299990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('COMCEL-HOGAR', 'HOGAR',299990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('UFF-HOGAR', 'HOGAR',299990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('UFF-DATOS', 'JARABE',299990, 50, 10, NOW());
INSERT INTO productos (nombre, tipo, precio, cantidad, cantidadm, create_at) VALUES('UFF-VOZ', 'VOZ',299990, 50, 10, NOW());